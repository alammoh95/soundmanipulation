#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void addecho(int d, int v, char* source, char* dest){
FILE *fp;
FILE *nfp;
fp = fopen(source, "rb");
//Check if file valid
if (fp == NULL){
	printf("Source File does not exist");
	exit(1);
}
nfp = fopen(dest, "w+");
int header[11];
int x;
//Write Header
fread(&header[0], sizeof(int), 11, fp);
header[1] = (unsigned int)(header[1] + (d * 2));
header[10] = (unsigned int)(header[10] + (d * 2));
fwrite(&header[0], sizeof(int), 11, nfp);

//Create buffer
short *buffer;
buffer = (short *)malloc(d * sizeof(short));
fread(&buffer[0], sizeof(short), d, fp);
fwrite(&buffer[0],sizeof(short), d, nfp);

//Scale buffer
for(x = 0; x < d; x++){
	buffer[x] = buffer[x] / v;
}

//Create first sample
short scaled[1];
scaled[0] = buffer[0]; //Take first sample from echo buffer
short sample[1]; //Sample from original
short mix[1]; // Original sample mixed with buffer sample
int numsamp = 0;

//Mix in samples
while (fread(&sample[0], sizeof(short), 1, fp) == 1){
	numsamp++;
	for (x = 0; x < d; x++){
		buffer[x] = buffer[x+1]; //Move everything one up from the buffer
		}
		buffer[d-1] = sample[0] / v; //Take one sample from orig,add to buffer
		mix[0] = scaled[0] + sample[0]; //Mix sample with buffer
		fwrite(&mix[0],sizeof(short), 1, nfp); //Write mixed sample to file
		scaled[0] = buffer[0]; //Take first sample from echo buffer
}

	//Write 0 samples
if ((d - numsamp) > 0){
	short empty[1]; //Create 0 sample
	empty[1] = 0;
	for(x = 0; x <= (d - numsamp); x++){
		fwrite(&empty[0],sizeof(short), 1, nfp); //Write 0 sample to file
	} 
}
//Add	
fwrite(&buffer[0],sizeof(short), d, nfp);
}


int main(int argc, char* argv[]){
	int c;
	int d = 8000;
	int v = 4;
	int num = 0;
	//Assess Options
	while ((c = getopt (argc, argv, "d:v:")) != -1){
		switch (c){
			case 'd' :
				d = atoi(optarg);
				num=+2;
				break;
			case 'v' :
				v = atoi(optarg);
				num+=2;
				break;
		}
	}
	//Add echo
	if (argc == (num + 3)){
		addecho(d, v, argv[num + 1], argv[num + 2]);
	}
	else{
		printf("Argument Error");
	}
	return 0;
	}